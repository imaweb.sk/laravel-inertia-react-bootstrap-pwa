<!doctype html>
<html lang="en">
    <head>

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- Meta tags --}}
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/jpg" href="favicon.png"/>

        {{-- Insertia head --}}
        @inertiaHead

        {{-- Bootstrap --}}
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
            crossorigin="anonymous"
        >

        {{-- Styles --}}
        <link
            href="{{ mix('/css/app.css') }}"
            rel="stylesheet"
        />

        {{-- Manifest --}}
        <link rel="manifest" href="/manifest.webmanifest">

    </head>
    <body>

        {{-- Inertia content --}}
        @inertia

        {{-- Bootstrap --}}
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"
        ></script>

        {{-- Scripts --}}
        <script
            src="{{ mix('/js/app.js') }}"
            defer
        ></script>

        {{-- PWA Serviceworker --}}
        <script>
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register('/service-worker.js');
            }
        </script>

    </body>
</html>
