import React, { useEffect } from 'react'
import Navigation from "../components/Navigation";
import Footer from "../components/Footer";
import Page from "./Page";

export default function Layout(content) {
    return (
        <React.Fragment>
            <Navigation></Navigation>
            <Page content={content} />
            <Footer></Footer>
        </React.Fragment>
    )
}

