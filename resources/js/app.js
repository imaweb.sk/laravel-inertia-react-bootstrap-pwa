/** Import React */
import React from 'react'
import { render } from 'react-dom'

/** Import Inertia */
import { createInertiaApp } from '@inertiajs/inertia-react'
import { InertiaProgress } from '@inertiajs/progress'
import Layout from './Layout/Layout'

/** Add page loading bar */
InertiaProgress.init()

/** Resolver for binding routes to pages */
createInertiaApp({
    resolve: name => {
        const page = require(`./Pages/${name}`).default
        page.layout = page.layout || Layout
        return page
    },
    setup({el,App,props}){
        render(<React.StrictMode><App {...props} /></React.StrictMode>, el)
    },
});

/** PWA Installation */
window.addEventListener('DOMContentLoaded', (event) => {
    let deferredPrompt;
    window.addEventListener('beforeinstallprompt', (e) => {
        e.preventDefault();
        deferredPrompt = e;
        document.getElementById('install-pwa').classList.remove('d-none');
    });
    document.getElementById('install-pwa').addEventListener('click', (e) => {
        deferredPrompt.prompt();
        deferredPrompt.userChoice.then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
                document.getElementById('install-pwa').classList.add('d-none');
            }
        });
    });
});
