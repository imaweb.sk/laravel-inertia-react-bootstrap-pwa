import { Inertia } from '@inertiajs/inertia'
import React, { useState } from 'react'
import { Link } from "@inertiajs/inertia-react";

export default function Footer() {
    return (
        <footer className="border-top pt-4 pb-4">
            <div className="container">
                <button id="install-pwa" className="d-none btn btn-secondary">Install PWA</button>
            </div>
        </footer>
    )
}
