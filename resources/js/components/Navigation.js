import { Inertia } from '@inertiajs/inertia'
import {Link, usePage} from "@inertiajs/inertia-react";
import React, { useState } from 'react'

export default function Navigation() {

    const { auth } = usePage().props

    return (

        <nav
            className="navbar navbar-expand-lg navbar-light border-bottom"
        >
            <div
                className="container"
            >
                <Link
                    className="navbar-brand"
                    href="/"
                >
                    Laravel
                </Link>
                <button
                    className="bg-transparent border-0 d-lg-none p-0"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarScroll"
                    aria-controls="navbarScroll"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span
                        className="navbar-toggler-icon"
                    ></span>
                </button>
                <div
                    className="collapse navbar-collapse justify-content-end"
                    id="navbarScroll"
                >
                    {
                        auth.user ? (
                            <ul
                                className="navbar-nav"
                            >
                                <li>
                                    <Link
                                        className="nav-link text-black"
                                        href="/home"
                                    >
                                        Home
                                    </Link>
                                </li>
                                <li
                                    className="nav-item dropdown"
                                >
                                    <a
                                        className="nav-link dropdown-toggle text-black"
                                        href="#"
                                        id="navbarScrollingDropdown"
                                        role="button"
                                        data-bs-toggle="dropdown"
                                        aria-expanded="false"
                                    >
                                        {auth.user.name}
                                    </a>
                                    <ul
                                        className="dropdown-menu"
                                        aria-labelledby="navbarScrollingDropdown"
                                    >
                                        <li>
                                            <Link
                                                href="/profile"
                                                className="dropdown-item text-black"
                                            >
                                                Profile
                                            </Link>
                                        </li>
                                        <li>
                                            <Link
                                                href="/logout"
                                                method="post"
                                                as="button"
                                                type="button"
                                                className="dropdown-item text-black"
                                            >
                                                Logout
                                            </Link>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        ) : (
                            <ul
                                className="navbar-nav"
                            >
                                <li
                                    className="nav-item"
                                >
                                    <Link
                                        href="/login"
                                        className="text-black text-decoration-none ms-0 ms-lg-4"
                                    >
                                        Login
                                    </Link>
                                </li>
                                <li
                                    className="nav-item"
                                >
                                    <Link
                                        href="/register"
                                        className="text-black text-decoration-none ms-0 ms-lg-4"
                                    >
                                        Register
                                    </Link>
                                </li>
                            </ul>
                        )
                    }
                </div>
            </div>
        </nav>
    )
}
