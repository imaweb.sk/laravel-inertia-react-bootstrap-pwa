import React, { useState } from 'react'
import { Inertia } from "@inertiajs/inertia";
import {Head} from "@inertiajs/inertia-react";

export default function Register() {

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");

    const [errors, setErrors] = useState({
        "name": "",
        "email": "",
        "password": "",
        "repeatPassword": ""
    });

    function handleSubmit(e) {
        e.preventDefault();
        Inertia.visit('/register',{
            method: 'post',
            data: {
                name: name,
                email: email,
                password: password,
                password_confirmation: repeatPassword
            },
            preserveState: true,
            preserveScroll: true,
            onError: errors => {
                setErrors({
                    "name": errors.name,
                    "email": errors.email,
                    "password": errors.password,
                    "repeatPassword": errors.password_confirmation
                })
            }
        });
    }

    return (
        <React.Fragment>
            <Head>
                <title>Register</title>
                <meta
                    head-key="description"
                    name="description"
                    content="Create new account"
                />
            </Head>
            <div className="container mt-4 mb-4">
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header p-3">
                                <h6>Create an account</h6>
                            </div>
                            <form onSubmit={handleSubmit}>
                                {
                                    errors.name || errors.email || errors.password || errors.repeatPassword ? (
                                            <div className="card-body bg-danger text-white">
                                                <b>Error occurred, fix the following:</b>
                                                <ul className="mb-0">
                                                    <div>{errors.name ? (<li>{errors.name}</li>) : null}</div>
                                                    <div>{errors.email ? (<li>{errors.email}</li>) : null}</div>
                                                    <div>{errors.password ? (<li>{errors.password}</li>) : null}</div>
                                                    <div>{errors.repeatPassword ? (<li>{errors.repeatPassword}</li>) : null}</div>
                                                </ul>
                                            </div>
                                        ) :
                                        null
                                }
                                <div className="card-body p-3">
                                    <input
                                        type="text"
                                        id="name"
                                        value={name}
                                        onChange={(e) => setName(e.target.value)}
                                        className="form-control mb-2"
                                        placeholder="Name"
                                    />
                                    <input
                                        type="email"
                                        id="email"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        className="form-control mb-2"
                                        placeholder="E-Mail"
                                    />
                                    <input
                                        type="password"
                                        id="password"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        className="form-control mb-2"
                                        placeholder="Password"
                                    />
                                    <input
                                        type="password"
                                        id="repeat-password"
                                        value={repeatPassword}
                                        onChange={(e) => setRepeatPassword(e.target.value)}
                                        className="form-control"
                                        placeholder="Repeat password"
                                    />
                                </div>
                                <div className="card-footer p-3">
                                    <button className="btn btn-secondary">Create account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
