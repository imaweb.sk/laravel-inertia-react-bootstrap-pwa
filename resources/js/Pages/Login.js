import { Inertia } from '@inertiajs/inertia'
import React, { useState } from 'react'
import { Head } from "@inertiajs/inertia-react";

export default function Edit() {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [errors, setErrors] = useState({
        "email": "",
        "password": ""
    });

    function handleSubmit(e) {
        e.preventDefault()
        Inertia.visit('/login',{
            method: 'post',
            data: {
                email: email,
                password: password
            },
            preserveState: true,
            preserveScroll: true,
            onError: errors => {
                setErrors({
                    "email": errors.email,
                    "password": errors.password
                })
            }
        });
    }

    return (
        <React.Fragment>
            <Head>
                <title>Login</title>
                <meta
                    head-key="description"
                    name="description"
                    content="Login to your account"
                />
            </Head>
            <div className="container mt-4 mb-4">
                <div className="row">
                    <div className="col-md-6">
                        <div className="card p-0">
                            <form onSubmit={handleSubmit}>
                                <div className="card-header p-3">
                                    <h6>Login</h6>
                                </div>
                                {
                                    errors.email || errors.password ? (
                                            <div className="card-body bg-danger text-white">
                                                <b>Error occurred, fix the following:</b>
                                                <ul className="mb-0">
                                                    <div>{errors.email ? (<li>{errors.email}</li>) : null}</div>
                                                    <div>{errors.password ? (<li>{errors.password}</li>) : null}</div>
                                                </ul>
                                            </div>
                                        ) :
                                        null
                                }
                                <div className="card-body p-3">
                                    <div>
                                        <input
                                            id="email"
                                            value={email}
                                            onChange={(e) => setEmail(e.target.value)}
                                            placeholder="E-Mail"
                                            className="form-control mb-2"
                                        />
                                    </div>
                                    <div>
                                        <input
                                            id="password"
                                            value={password}
                                            onChange={(e) => setPassword(e.target.value)}
                                            type="password"
                                            placeholder="Password"
                                            className="form-control"
                                        />
                                    </div>
                                </div>
                                <div className="card-footer p-3">
                                    <button className="btn btn-secondary" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
