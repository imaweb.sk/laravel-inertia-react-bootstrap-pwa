import React, { useState } from 'react'
import { Head } from "@inertiajs/inertia-react";

export default function Index({ children }) {
    return (
        <React.Fragment>
            <Head>
                <title>Index</title>
                <meta
                    head-key="description"
                    name="description"
                    content="Welcome"
                />
            </Head>
            <div className="container mt-4 mb-4">
                Index
            </div>
        </React.Fragment>
    )
}

