import React from 'react'
import { Head } from '@inertiajs/inertia-react'

export default function Home() {
    return (
        <React.Fragment>
            <Head>
                <title>Home</title>
                <meta
                    head-key="description"
                    name="description"
                    content="Home page"
                />
            </Head>
            <div className="container mt-4 mb-4 mt-4 mb-4">
                Home
            </div>
        </React.Fragment>
    )
}

