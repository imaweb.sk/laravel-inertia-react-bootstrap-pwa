import { Inertia } from '@inertiajs/inertia'
import React, { useState } from 'react'
import { Head, usePage } from "@inertiajs/inertia-react";

export default function Profile() {

    const { profile } = usePage().props;

    const [name, setName] = useState(profile.name);
    const [email, setEmail] = useState(profile.email);
    const [password, setPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");
    const [updateMessage, setUpdateMessage] = useState("");

    const [errors, setErrors] = useState({
        "name": "",
        "email": "",
        "password": "",
        "repeatPassword": ""
    });

    function handleSubmit(e) {
        e.preventDefault();
        Inertia.visit('/profile/update',{
            method: 'post',
            data: {
                name: name,
                email: email,
                password: password,
                password_confirmation: repeatPassword
            },
            preserveState: true,
            preserveScroll: true,
            onError: errors => {
                setUpdateMessage("");
                setErrors({
                    "name": errors.name,
                    "email": errors.email,
                    "password": errors.password,
                    "repeatPassword": errors.password_confirmation
                })
            },
            onSuccess: page => {
                setErrors({
                    "name": "",
                    "email": "",
                    "password": "",
                    "repeatPassword": ""
                });
                setUpdateMessage("Profile has been updated");
            },

        });
    }

    return (
        <React.Fragment>
            <Head>
                <title>Profile</title>
                <meta
                    head-key="description"
                    name="description"
                    content="Update your profile"
                />
            </Head>
            <div className="container mt-4 mb-4">
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header p-3">
                                <h6>Update profile</h6>
                            </div>
                            <form onSubmit={handleSubmit}>
                                {
                                    errors.name || errors.email || errors.password || errors.repeatPassword ? (
                                            <div className="card-body bg-danger text-white">
                                                <b>Error occurred, fix the following:</b>
                                                <ul className="mb-0">
                                                    <div>{errors.name ? (<li>{errors.name}</li>) : null}</div>
                                                    <div>{errors.email ? (<li>{errors.email}</li>) : null}</div>
                                                    <div>{errors.password ? (<li>{errors.password}</li>) : null}</div>
                                                    <div>{errors.repeatPassword ? (<li>{errors.repeatPassword}</li>) : null}</div>
                                                </ul>
                                            </div>
                                        ) :
                                        null
                                }
                                {
                                    updateMessage ? (
                                        <div className="card-body bg-success text-white">
                                            {updateMessage}
                                            <span role="button" className="float-end" onClick={(e) => setUpdateMessage("")}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                     fill="currentColor" className="bi bi-x-circle" viewBox="0 0 16 16">
                                                  <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                  <path
                                                      d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                                </svg>
                                            </span>
                                        </div>
                                    ) : null
                                }
                                <div className="card-body p-3">
                                    <input
                                        type="text"
                                        id="name"
                                        value={name}
                                        onChange={(e) => setName(e.target.value)}
                                        className="form-control mb-2"
                                        placeholder="Name"
                                    />
                                    <input
                                        type="email"
                                        id="email"
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                        className="form-control mb-2"
                                        placeholder="E-Mail"
                                    />
                                    <input
                                        type="password"
                                        id="password"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        className="form-control mb-2"
                                        placeholder="Password"
                                    />
                                    <input
                                        type="password"
                                        id="repeat-password"
                                        value={repeatPassword}
                                        onChange={(e) => setRepeatPassword(e.target.value)}
                                        className="form-control"
                                        placeholder="Repeat password"
                                    />
                                </div>
                                <div className="card-footer p-3">
                                    <button className="btn btn-secondary">Update account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
