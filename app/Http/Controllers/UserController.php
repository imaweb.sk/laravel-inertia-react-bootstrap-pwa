<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class UserController extends Controller
{

    public function Profile(): \Inertia\Response|\Inertia\ResponseFactory
    {
        return inertia("Profile", [
            "profile" => User::select("name", "email")->where('id', Auth::id())->first()
        ]);
    }

    public function updateProfile(Request $request): \Illuminate\Http\RedirectResponse
    {

        /** Validate input */
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255']
        ]);

        if($request->password) {
            $request->validate([
                'password' => ['string', 'min:8', 'confirmed']
            ]);
        }

        /** Update user */
        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password) {
            $user->password = Hash::make($request->password);
        }
        $user->save();

        /** Redirect back */
        return back();

    }

}
