<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function login(): \Illuminate\Routing\Redirector|\Inertia\Response|\Inertia\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        if(Auth::check()) {
            return redirect('/home');
        } else {
            return inertia('Login');
        }
    }

    public function register(): \Illuminate\Routing\Redirector|\Inertia\Response|\Inertia\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Contracts\Foundation\Application
    {
        if(Auth::check()) {
            return redirect('/home');
        } else {
            return inertia('Register');
        }
    }

}
